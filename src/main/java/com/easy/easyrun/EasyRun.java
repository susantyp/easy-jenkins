package com.easy.easyrun;

import cn.hutool.core.util.StrUtil;
import com.easy.EasyJenkinsApplication;
import com.easy.bean.DataStructure;
import com.easy.enums.EasyJenkinsEnum;
import com.easy.frame.MainFrame;
import com.easy.service.DataStructureService;
import com.easy.service.QueryDataStructureService;
import com.easy.util.PreferencesJenkinsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@Component
public class EasyRun {

    @Autowired
    QueryDataStructureService queryDataStructureService;

    public void run(String head, String localhost) {
        DataStructure dataStructure = queryDataStructureService.getDataStructure();
        try {
            String registryPath = PreferencesJenkinsUtil.get(EasyJenkinsEnum.EASY_JENKINS_PATH.getParam());
            String field = PreferencesJenkinsUtil.get(EasyJenkinsEnum.EASY_JENKINS_FILE_ID.getParam());
            if (dataStructure == null){
                dataStructure = new DataStructure();
                dataStructure.setEasyJenkinsPort(8329);
            }
            if (StrUtil.isNotBlank(registryPath) && StrUtil.isNotBlank(field)) {
                Runtime.getRuntime().exec("cmd   /c   start " + head + "://" + localhost + ":" + dataStructure.getEasyJenkinsPort() + " ");
            } else {
                Runtime.getRuntime().exec("cmd   /c   start " + head + "://" + localhost + ":" + dataStructure.getEasyJenkinsPort() + "/init");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
