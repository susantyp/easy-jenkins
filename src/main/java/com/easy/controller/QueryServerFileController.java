package com.easy.controller;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import com.easy.bean.vo.DirectorFileVo;
import com.easy.deploy.util.SftpUtil;
import com.easy.result.EasyJenkinsResult;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author typsusan
 * <p>des</p>
 **/
@RestController
@RequestMapping("/query/server/file")
public class QueryServerFileController {


    @PostMapping("/list")
    public EasyJenkinsResult fileList(@RequestBody JSONObject jsonObject){
        if (StrUtil.isBlank(jsonObject.getStr("directory"))){
            return EasyJenkinsResult.error("路径为空");
        }
        if (StrUtil.isBlank(jsonObject.getStr("host"))){
            return EasyJenkinsResult.error("请先在下拉框选择协议");
        }
        List<DirectorFileVo> directorFileVos = SftpUtil.listDirectories(jsonObject.getStr("host"),
                jsonObject.getInt("port"), jsonObject.getStr("username"),
                jsonObject.getStr("password"), jsonObject.getStr("directory"));
        return EasyJenkinsResult.success(directorFileVos);
    }


}
