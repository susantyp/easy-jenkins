package com.easy.controller;

import cn.hutool.core.util.StrUtil;
import com.easy.bean.DataStructure;
import com.easy.bean.DeployRecord;
import com.easy.enums.EasyJenkinsEnum;
import com.easy.service.DataStructureService;
import com.easy.util.PreferencesJenkinsUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 * @author typsusan
 * <p>des</p>
 **/
@RestController
@RequestMapping("/init")
public class InitSystemController {


    @PostMapping("/system")
    public ResponseEntity<Map<String, Object>> deploy(@RequestBody DataStructure dataStructure) {

        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("code", 200);
        objectMap.put("msg", "成功");

        if (isPortInUse(dataStructure.getEasyJenkinsPort())){
            objectMap.put("code", 500);
            objectMap.put("msg", "当前"+dataStructure.getEasyJenkinsPort()+"端口被占用");
            return ResponseEntity.ok(objectMap);
        }

        String registryPath = PreferencesJenkinsUtil.get(EasyJenkinsEnum.EASY_JENKINS_PATH.getParam());
        String field = PreferencesJenkinsUtil.get(EasyJenkinsEnum.EASY_JENKINS_FILE_ID.getParam());
        if (StrUtil.isNotBlank(registryPath) && StrUtil.isNotBlank(field)){
            objectMap.put("code", 500);
            objectMap.put("msg", "当前系统已初始化，无需配置，如需修改，请到设置中修改");
            return ResponseEntity.ok(objectMap);
        }

        DataStructureService.initJSON(dataStructure.getDataPath(),dataStructure.getMavenPath(),dataStructure.getEasyJenkinsPort());

        return ResponseEntity.ok(objectMap);
    }

    public boolean isPortInUse(int port) {
        if (port == 8329){
            return false;
        }
        try (Socket ignored = new Socket("localhost", port)) {
            return true;
        } catch (IOException e) {
            return false;
        }
    }

}
