package com.easy.controller;
import com.easy.deploy.util.SftpUtil;
import com.easy.enums.EasyJenkinsEnum;
import com.easy.state.SingletonState;
import com.easy.util.DESUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;

import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.connection.channel.direct.Session;
import net.schmizz.sshj.transport.verification.PromiscuousVerifier;

/**
 * @author typsusan
 * <p>des</p>
 **/
@RestController
public class LogController {


    private final ConcurrentHashMap<String, AtomicReference<SseEmitter>> activeEmitters = new ConcurrentHashMap<>();
    private final ExecutorService executor = Executors.newCachedThreadPool();

    @GetMapping("/log-stream")
    public SseEmitter streamLogs(
            @RequestParam String hostname,
            @RequestParam int port,
            @RequestParam String username,
            @RequestParam String password,
            @RequestParam String connectId) {
        AtomicReference<SseEmitter> previousEmitterRef = activeEmitters.get(connectId);
        if (previousEmitterRef != null) {
            SseEmitter previousEmitter = previousEmitterRef.get();
            if (previousEmitter != null) {
                previousEmitter.complete();
            }
        }

        SseEmitter emitter = new SseEmitter(Long.MAX_VALUE);
        activeEmitters.put(connectId, new AtomicReference<>(emitter));

        executor.submit(() -> {
            final SSHClient sshClient = new SSHClient();
            sshClient.addHostKeyVerifier(new PromiscuousVerifier());
            try {
                sshClient.connect(hostname, port);
                sshClient.authPassword(username, DESUtil.decrypt(password));

                try (Session session = sshClient.startSession()) {
                    String command = "tail -f " + EasyJenkinsEnum.EASY_SERVER_LOG_PATH.getParam();
                    Session.Command cmd = session.exec(command);

                    try (BufferedReader reader = new BufferedReader(new InputStreamReader(cmd.getInputStream()));
                         BufferedReader errorReader = new BufferedReader(new InputStreamReader(cmd.getErrorStream()))) {
                        String line;
                        while ((line = reader.readLine()) != null) {
                            emitter.send(SseEmitter.event().data(line));
                        }
                        while ((line = errorReader.readLine()) != null) {
                            emitter.send(SseEmitter.event().data("ERROR: " + line));
                        }
                        emitter.complete();
                    }
                }
            } catch (IOException e) {
                emitter.completeWithError(e);
            } finally {
                try {
                    sshClient.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        return emitter;
    }
}