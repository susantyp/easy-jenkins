package com.easy.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.easy.bean.DataBranch;
import com.easy.bean.DataHost;
import com.easy.bean.DataStructure;
import com.easy.bean.vo.HostVo;
import com.easy.deploy.util.SftpUtil;
import com.easy.deploy.vo.DeployConnect;
import com.easy.service.DataStructureService;
import com.easy.service.QueryDataStructureService;
import com.easy.util.DESUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author typsusan
 * <p>des</p>
 **/
@RestController
@RequestMapping("/host")
public class HostController {


    @Autowired
    QueryDataStructureService queryDataStructureService;

    @PostMapping("/save")
    public ResponseEntity<Map<String, Object>> save(@RequestBody DataHost dataHost) {
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("code", 200);
        objectMap.put("msg", "成功");
        String time = DateUtil.now();
        dataHost.setCreateTime(time);
        if (StrUtil.isBlank(dataHost.getHost())) {
            objectMap.put("code", 500);
            objectMap.put("msg", "服务器地址不能为空");
            return ResponseEntity.ok(objectMap);
        }
        DataStructure dataStructure = queryDataStructureService.getDataStructure();
        Snowflake snowflake = IdUtil.getSnowflake(1, 1);
        String serverId = snowflake.nextIdStr();
        dataHost.setServerId(serverId);
        String pwd = DESUtil.encryption(dataHost.getPassword());
        dataHost.setPassword(pwd);
        List<DataHost> hostList = DataStructureService.initFile(dataStructure.getDataPath(), "host", "easy-host", DataHost.class);
        hostList.add(dataHost);
        DataStructureService.setFile(dataStructure.getDataPath(), "host", "easy-host", hostList);
        objectMap.put("time",time);
        objectMap.put("serverId",serverId);
        objectMap.put("pwd",pwd);
        return ResponseEntity.ok(objectMap);
    }

    @PostMapping("/update")
    public ResponseEntity<Map<String, Object>> update(@RequestBody DataHost dataHost) {
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("code", 200);
        objectMap.put("msg", "成功");
        String time = DateUtil.now();
        dataHost.setCreateTime(time);
        if (StrUtil.isBlank(dataHost.getHost())) {
            objectMap.put("code", 500);
            objectMap.put("msg", "服务器地址不能为空");
            return ResponseEntity.ok(objectMap);
        }
        DataStructure dataStructure = queryDataStructureService.getDataStructure();
        List<DataHost> hostList = DataStructureService.initFile(dataStructure.getDataPath(), "host", "easy-host", DataHost.class);
        boolean anIf = hostList.removeIf(s -> s.getServerId().equals(dataHost.getServerId()));
        queryDataStructureService.setDataStructure(dataStructure);
        hostList.add(dataHost);
        DataStructureService.setFile(dataStructure.getDataPath(), "host", "easy-host", hostList);
        return ResponseEntity.ok(objectMap);
    }

    @GetMapping("/delete/{serverId}")
    public ResponseEntity<Map<String, Object>> delete(@PathVariable String serverId) {
        Map<String, Object> objectMap = new HashMap<>();
        if (StrUtil.isBlank(serverId)){
            objectMap.put("code", 500);
            objectMap.put("msg", "服务器id 不能为空");
            return ResponseEntity.ok(objectMap);
        }
        DataStructure dataStructure = queryDataStructureService.getDataStructure();
        List<DataHost> hostList = DataStructureService.initFile(dataStructure.getDataPath(), "host", "easy-host", DataHost.class);
        boolean anIf = hostList.removeIf(s -> s.getServerId().equals(serverId));
        queryDataStructureService.setDataStructure(dataStructure);
        DataStructureService.setFile(dataStructure.getDataPath(), "host", "easy-host", hostList);
        return ResponseEntity.ok(objectMap);
    }

    @PostMapping("/test/connection")
    public ResponseEntity<Map<String, Object>> connection(@RequestBody DataHost dataHost) {
        HostVo hostVo = SftpUtil.sSHConnection(dataHost.getHost(), dataHost.getPort(), dataHost.getUsername(), dataHost.getPassword());
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("host",hostVo);
        return ResponseEntity.ok(objectMap);
    }
}
