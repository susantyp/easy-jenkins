package com.easy.convers;
import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.easy.bean.vo.MsgVo;
import com.easy.state.SingletonState;
import com.easy.websocket.WebSocketServer;
import com.sun.org.apache.regexp.internal.RE;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * @author typsusan
 * <p>des</p>
 **/
@Slf4j
public class CustomMessageConverter extends ClassicConverter  {

    @Override
    public String convert(ILoggingEvent iLoggingEvent) {
        String maskedMessage = iLoggingEvent.getFormattedMessage();
        try {
            MsgVo msgVo = JSONUtil.toBean(maskedMessage, MsgVo.class); // 将消息转换成 MsgVo 对象
            JSONObject object = new JSONObject(); // 创建一个新的 JSONObject
            object.set(msgVo.getKey(), msgVo.getMsg()); // 设置键值对
            String jsonString = object.toString(); // 转换为 JSON 字符串
            WebSocketServer.send(jsonString);
            return jsonString;
        } catch (Exception e) {
            log.error("消息转换失败: {}", e.getMessage(), e); // 记录错误日志
            return maskedMessage; // 如果转换失败，返回原始消息
        }
    }


}