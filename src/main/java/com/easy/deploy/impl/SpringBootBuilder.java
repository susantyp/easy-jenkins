package com.easy.deploy.impl;


import com.easy.annotation.tag;
import com.easy.deploy.abs.DeployBuilder;
import com.easy.deploy.util.LogUtil;
import com.easy.deploy.util.MavenUtil;
import com.easy.deploy.util.SftpUtil;
import com.easy.enums.EasyJenkinsEnum;

import java.io.IOException;
import java.util.HashSet;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@tag(name = "springboot")
public class SpringBootBuilder extends DeployBuilder {

    @Override
    public void mavenClean() {
        LogUtil.info("开始执行maven命令：clean", deployConnect.getConnectId(), deployConnect.getHost());
        MavenUtil.mvn(deployConnect.getMavenPath(), deployConnect.getPomXmlPath(), "clean", deployConnect.getConnectId(), deployConnect.getHost());
        LogUtil.info("清除完成", deployConnect.getConnectId(), deployConnect.getHost());
    }

    @Override
    public void mavenInstall() {
        LogUtil.info("开始执行maven命令：install", deployConnect.getConnectId(), deployConnect.getHost());
        MavenUtil.mvn(deployConnect.getMavenPath(), deployConnect.getPomXmlPath(), "install", deployConnect.getConnectId(), deployConnect.getHost());
        LogUtil.info("打包完成", deployConnect.getConnectId(), deployConnect.getHost());
    }

    @Override
    public void clearNumber() throws IOException {
        LogUtil.info("强制杀死进程.....", deployConnect.getConnectId(), deployConnect.getHost());
        SftpUtil.exec(
                deployConnect.getHost(),
                deployConnect.getPort(),
                deployConnect.getUsername(),
                deployConnect.getPassword(),
                "pkill -f " + deployConnect.getJarName()
                ,deployConnect.getConnectId(),null);
        LogUtil.info("清理完成.....", deployConnect.getConnectId(), deployConnect.getHost());
    }


    @Override
    public void delete() {
        LogUtil.info("文件开始删除", deployConnect.getConnectId(), deployConnect.getHost());
        SftpUtil.delete(deployConnect.getHost(),
                deployConnect.getPort(),
                deployConnect.getUsername(),
                deployConnect.getPassword(),
                deployConnect.getServerPath(),
                deployConnect.getJarName());
        LogUtil.info("删除完成", deployConnect.getConnectId(), deployConnect.getHost());
    }

    @Override
    public void uploading() {
        LogUtil.info("文件开始上传...", deployConnect.getConnectId(), deployConnect.getHost());
        SftpUtil.uploadJar(deployConnect.getHost(),
                deployConnect.getPort(),
                deployConnect.getUsername(),
                deployConnect.getPassword(),
                deployConnect.getServerPath() + "/" + deployConnect.getJarName(),
                deployConnect.getLocalPath(), deployConnect.getConnectId());
    }

    @Override
    public void run() throws IOException {
        LogUtil.info("开始部署", deployConnect.getConnectId(), deployConnect.getHost());
        LogUtil.info("当前执行命令：" + deployConnect.getExec(), deployConnect.getConnectId(), deployConnect.getHost());
        SftpUtil.exec(
                deployConnect.getHost(),
                deployConnect.getPort(),
                deployConnect.getUsername(),
                deployConnect.getPassword(), deployConnect.getExec(),deployConnect.getConnectId(),EasyJenkinsEnum.EAST_SERVER_EXEC_SPRINGBOOT.getParam());
        LogUtil.info(EasyJenkinsEnum.SUCCESSFULLY_DEPLOYED.getParam(), deployConnect.getConnectId(), deployConnect.getHost());
    }
}
