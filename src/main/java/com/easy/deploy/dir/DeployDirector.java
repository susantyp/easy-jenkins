package com.easy.deploy.dir;


import cn.hutool.core.thread.ThreadFactoryBuilder;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.easy.annotation.IfMapper;
import com.easy.annotation.find.ReturnObject;
import com.easy.deploy.abs.DeployBuilder;
import com.easy.deploy.util.LogUtil;
import com.easy.deploy.vo.DeployConnect;
import com.easy.service.QueryDataStructureService;
import org.apache.tomcat.util.threads.ThreadPoolExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@IfMapper(pack = "com.easy.deploy.impl")
@Component
public class DeployDirector {

    DeployBuilder deployBuilder;

    @Autowired
    QueryDataStructureService queryDataStructureService;

    @Async
    public void start(DeployConnect deployConnect) {
        this.deployBuilder = ReturnObject.getObj(DeployDirector.class, deployConnect.getTypeName());
        this.deployBuilder.setDeployConnect(deployConnect);
        try {
            deployBuilder.mavenClean();
            deployBuilder.mavenInstall();
            deployBuilder.npmBuild();
            deployBuilder.clearNumber();
            deployBuilder.processNumber();
            deployBuilder.closeProcess();
            deployBuilder.delete();
            deployBuilder.initDeployPath();
            deployBuilder.mkdir();
            deployBuilder.uploading();
            deployBuilder.run();
            deployBuilder.saveDeployRecord();
        } catch (IOException e) {
            LogUtil.info("[EASY JENKINS ERROR] "+e.getMessage(),deployConnect.getConnectId(),deployConnect.getHost(),2000);
            e.printStackTrace();
        }
    }


}
