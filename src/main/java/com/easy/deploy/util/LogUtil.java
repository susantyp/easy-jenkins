package com.easy.deploy.util;

import cn.hutool.json.JSONUtil;
import com.easy.bean.vo.MsgVo;
import com.easy.enums.EasyJenkinsEnum;
import com.easy.state.SingletonState;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author tanyongpeng
 * <p></p>
 **/
public class LogUtil {
    private static final Logger logger = Logger.getLogger(EasyJenkinsEnum.EASY_JENKINS.getParam());

    public static void severe(String msg) {
        logger.log(Level.SEVERE, msg);
    }

    public static void severe(String msg,Throwable throwable) {
        logger.log(Level.SEVERE, msg,throwable);
    }

    public static void severe(Throwable throwable,String connectId, String host) {
        String str = JSONUtil.toJsonStr(new MsgVo(host + "=" + connectId, throwable.getMessage()));
        logger.log(Level.SEVERE, str,throwable);
    }

    public static void warning(String msg) {
        logger.log(Level.WARNING, msg);
    }

    public static void info(String msg) {
        logger.log(Level.INFO, msg);
    }

    public static void info(String msg, String connectId, String host,int sleep){
        String str = JSONUtil.toJsonStr(new MsgVo(host + "=" + connectId, msg));
        logger.log(Level.INFO, str);
    }

    public static void info(String msg, String connectId, String host){
        info(msg,connectId,host,0);
    }

    public static boolean getDeployState(String connectId) {
        return SingletonState.getInstance().getFlag();
    }

    public static boolean isStart(){
       return SingletonState.getInstance().getFlag();
    }

}
