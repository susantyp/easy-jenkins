package com.easy.deploy.util;

import org.apache.maven.shared.invoker.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.util.Collections;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
public class MavenUtil {

    public static void mvn(String mavenPath, String pomXmlPath, String exec, String connectId, String host) {
        InvocationRequest request = new DefaultInvocationRequest();
        request.setPomFile(new File(pomXmlPath));
        request.setGoals(Collections.singletonList(exec));
        Invoker invoker = new DefaultInvoker();
        invoker.setMavenHome(new File(mavenPath));
        invoker.setLogger(new PrintStreamLogger(System.err, InvokerLogger.ERROR));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream capturePrintStream = new PrintStream(baos);
        invoker.setOutputHandler(capturePrintStream::println);
        try {
            invoker.execute(request);
            String capturedOutput = baos.toString();
            String[] split = capturedOutput.split("\\r\\n");
            for (String out : split) {
                LogUtil.info(out, connectId, host,300);
            }
        } catch (MavenInvocationException e) {
            e.printStackTrace();
            LogUtil.info("执行maven命令报错："+e.getMessage(), connectId, host);
        }
    }
}
