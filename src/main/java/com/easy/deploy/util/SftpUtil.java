package com.easy.deploy.util;


import cn.hutool.core.util.StrUtil;
import com.easy.bean.vo.DirectorFileVo;
import com.easy.bean.vo.HostVo;
import com.easy.deploy.vo.DeployPath;
import com.easy.enums.EasyJenkinsEnum;
import com.easy.state.SingletonState;
import com.easy.util.DESUtil;
import com.jcraft.jsch.*;
import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.common.IOUtils;
import net.schmizz.sshj.connection.channel.direct.Session;
import net.schmizz.sshj.transport.verification.PromiscuousVerifier;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
public class SftpUtil {
    private static final int TIMEOUT_SECONDS = 10; // 设定的超时时间

    public static SSHClient getSSHClient() {
        return new SSHClient();
    }

    public static void clearSSHClient(SSHClient sshClient) {
        if (sshClient != null) {
            try {
                sshClient.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static Set<String> exec(String host, int port, String username, String password, String e, Set<String> resultArray) throws IOException {
        final SSHClient ssh = new SSHClient();
        ssh.addHostKeyVerifier(new PromiscuousVerifier());
        ssh.connect(host, port);
        InputStream inputStream = null;
        OutputStream output = null;
        String decrypt = DESUtil.decrypt(password);
        System.out.println(decrypt);
        ssh.authPassword(username, decrypt);
        try (Session session = ssh.startSession()) {
            final Session.Command cmd = session.exec(e);
            inputStream = cmd.getInputStream();
            output = IOUtils.readFully(inputStream);
            if (!output.toString().trim().equals("")) {
                return CharUtil.getProcessNumber(output.toString(), resultArray);
            } else {
                return resultArray;
            }
        } catch (Exception e1) {
            LogUtil.severe("进程号获取失败============",e1);
            e1.printStackTrace();
            return resultArray;
        } finally {
            // 处理异常
            ssh.disconnect();
            if (inputStream != null) {
                inputStream.close();
            }
            if (output != null) {
                output.close();
            }
        }
    }



    public static void exec(String host, int port, String username, String password, String command,String id,String type) throws IOException {
        final SSHClient sshClient = getSSHClient();
        StringBuilder output = new StringBuilder();
        try {
            // 确保SSH客户端已连接
            if (!sshClient.isConnected()) {
                sshClient.addHostKeyVerifier(new PromiscuousVerifier());
                sshClient.connect(host, port);
                sshClient.authPassword(username, DESUtil.decrypt(password));
            }
            try (Session session = sshClient.startSession()) {
                Session.Command cmd;
                // jar 执行输出日志
                if (EasyJenkinsEnum.EAST_SERVER_EXEC_SPRINGBOOT.getParam().equals(type)){
                    cmd = session.exec(command + "> "+ EasyJenkinsEnum.EASY_SERVER_LOG_PATH.getParam()+" 2>&1");
                    readAndPrintLog(host,port,username,password,EasyJenkinsEnum.EASY_SERVER_LOG_PATH.getParam(),id);
                }else {
                    cmd = session.exec(command);
                }
                // 程序输出日志
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(cmd.getInputStream()));
                     BufferedReader errorReader = new BufferedReader(new InputStreamReader(cmd.getErrorStream()))) {
                    String line;
                    while ((line = reader.readLine()) != null) {
                        output.append(line).append(System.lineSeparator());
                    }
                    while ((line = errorReader.readLine()) != null) {
                        output.append("ERROR: ").append(line).append(System.lineSeparator());
                    }
                    LogUtil.info(output.toString(), id, host);
                    cmd.join(); // 等待命令完成
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                LogUtil.severe("命令执行失败！",ex);
            }
        } finally {
            if (sshClient.isConnected()) {
                sshClient.disconnect();
            }
            SftpUtil.clearSSHClient(sshClient);
        }
    }


    public static void readAndPrintLog(String host, int port, String username, String password, String logFilePath, String id) throws IOException {
        final SSHClient sshClient = new SSHClient();
        sshClient.addHostKeyVerifier(new PromiscuousVerifier());
        ExecutorService executorService = Executors.newFixedThreadPool(2);

        try {
            sshClient.connect(host, port);
            sshClient.authPassword(username, DESUtil.decrypt(password));

            try (Session session = sshClient.startSession()) {
                String command = "tail -f " + logFilePath;
                Session.Command cmd = session.exec(command);

                try (BufferedReader reader = new BufferedReader(new InputStreamReader(cmd.getInputStream()));
                     BufferedReader errorReader = new BufferedReader(new InputStreamReader(cmd.getErrorStream()))) {

                    Future<Boolean> outputFuture = executorService.submit(() -> {
                        boolean hasOutput = false;
                        try {
                            String line;
                            while ((line = reader.readLine()) != null) {
                                LogUtil.info(line, id, host);
                                hasOutput = true;
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        return hasOutput;
                    });

                    Future<Boolean> errorFuture = executorService.submit(() -> {
                        boolean hasOutput = false;
                        try {
                            String line;
                            while ((line = errorReader.readLine()) != null) {
                                LogUtil.info("ERROR: " + line, id, host);
                                hasOutput = true;
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        return hasOutput;
                    });

                    boolean hasOutput = true;

                    while (hasOutput) {
                        try {
                            hasOutput = outputFuture.get(TIMEOUT_SECONDS, TimeUnit.SECONDS) || errorFuture.get(TIMEOUT_SECONDS, TimeUnit.SECONDS);
                            if (!hasOutput) {
                                SingletonState.getInstance().setFlag(false,id,host);
                                break;
                            }
                        } catch (TimeoutException e) {
                            SingletonState.getInstance().setFlag(false,id,host);
                            hasOutput = false; // 超时表示无新输出
                        } catch (Exception e) {
                            e.printStackTrace();
                            break;
                        }
                    }

                    cmd.join(); // 等待命令完成
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            executorService.shutdownNow();
            if (sshClient.isConnected()) {
                sshClient.disconnect();
            }
        }
    }

    public static void delete(String host, int port, String username, String password, String servicePath, String fileName) {
        ChannelSftp sftp = null;
        com.jcraft.jsch.Session session = null;
        try {
            JSch jsch = new JSch();
            session = jsch.getSession(username, host, port);
            session.setPassword(DESUtil.decrypt(password));
            Properties config = new Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            Channel channel = session.openChannel("sftp");
            channel.connect();
            sftp = (ChannelSftp) channel;
            sftp.cd(servicePath);
            sftp.rm(fileName);
        } catch (JSchException | SftpException e) {
            e.printStackTrace();
            LogUtil.severe("文件删除失败！或许预期文件不存可忽略",e);
        } finally {
            if (sftp != null) {
                if (sftp.isConnected()) {
                    sftp.disconnect();
                }
            }
            if (session != null) {
                if (session.isConnected()) {
                    session.disconnect();
                }
            }
        }
    }

    public static void uploadJar(String host, int port, String username, String password, String serverPath, String localPath, String connectId) {
        ChannelSftp sftp = null;
        com.jcraft.jsch.Session session = null;
        try (FileInputStream fileInputStream = new FileInputStream(new File(localPath))) {
            JSch jsch = new JSch();
            session = jsch.getSession(username, host, port);
            session.setPassword(DESUtil.decrypt(password));
            Properties config = new Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();

            Channel channel = session.openChannel("sftp");
            channel.connect();
            sftp = (ChannelSftp) channel;

            LogUtil.info("开始上传文件：" + serverPath, connectId, host);

            long fileSize = new File(localPath).length();
            sftp.put(fileInputStream, serverPath, new EasyProgressMonitor(fileSize, connectId, host), ChannelSftp.OVERWRITE);

        } catch (JSchException | SftpException | IOException e) {
            e.printStackTrace();
            LogUtil.severe("上传文件失败！",e);
        } finally {
            if (sftp != null) {
                if (sftp.isConnected()) {
                    sftp.disconnect();
                }
            }
            if (session != null) {
                if (session.isConnected()) {
                    session.disconnect();
                }
            }
        }
    }

    private static class EasyProgressMonitor implements SftpProgressMonitor {
        private long max;
        private long count = 0;
        private long percent = -1;
        private String connectId;
        private String host;

        EasyProgressMonitor(long max, String connectId, String host) {
            this.max = max;
            this.connectId = connectId;
            this.host = host;
        }

        public void init(int op, String src, String dest, long max) {
            // 初始化进度监控器
        }

        public boolean count(long count) {
            this.count += count;
            long percentNow = this.count * 100 / max;
            if (percentNow > this.percent) {
                this.percent = percentNow;
                if (this.percent >= 99){
                    LogUtil.info("上传进度：" + this.percent + "%", connectId, host);
                }else {
                    LogUtil.info("上传进度：" + this.percent + "%", connectId, host,0);
                }
            }
            return true;
        }

        public void end() {
            LogUtil.info("上传完成", connectId, host);
        }
    }

    public static void uploadFiles(String host, int port, String username, String password,String connectId,
                                   String localDir, String remoteDir){
        ChannelSftp sftp = null;
        Channel channel = null;
        com.jcraft.jsch.Session session = null;
        try {
            // 创建JSch对象
            JSch jsch = new JSch();
            // 连接远程服务器
            session = jsch.getSession(username, host, port);
            session.setPassword(DESUtil.decrypt(password));
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect();
            // 获取SFTP通道
            channel = session.openChannel("sftp");
            channel.connect();
            sftp = (ChannelSftp) channel;
            // 如果远程目录不存在，则创建目录
            try {
                sftp.stat(remoteDir);
            } catch (SftpException e) {
                if (e.id == ChannelSftp.SSH_FX_NO_SUCH_FILE) {
                    sftp.mkdir(remoteDir);
                }
            }
            // 递归上传文件
            File localFile = new File(localDir);
            if (localFile.isDirectory()) {
                uploadDirectory(sftp, localFile, remoteDir,connectId,host);
            } else {
                uploadFile(sftp, localFile, remoteDir);
            }
        }catch (JSchException|SftpException e){
            e.printStackTrace();
            LogUtil.severe("文件批量上传失败！",e);
        }finally {
            // 关闭SFTP通道和SSH连接
            if (sftp != null){
                sftp.exit();
            }
            if (channel != null){
                channel.disconnect();
            }
            if (session != null){
                session.disconnect();
            }
        }

    }

    private static void uploadDirectory(ChannelSftp sftp, File localDir, String remoteDir,String connectId,String host) throws SftpException {
        // 如果远程目录不存在，则创建目录
        try {
            sftp.stat(remoteDir);
        } catch (SftpException e) {
            if (e.id == ChannelSftp.SSH_FX_NO_SUCH_FILE) {
                sftp.mkdir(remoteDir);
                LogUtil.info("开始创建文件夹");
            }
        }
        // 递归上传目录下的文件和子目录
        File[] files = localDir.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                String name = StrUtil.isBlank(file.getName())?"正在上传根目录文件":"上传文件索引 "+file.getName()+"...";
                LogUtil.info(name,connectId,host);
                String remoteSubDir = remoteDir + "/" + file.getName();
                uploadDirectory(sftp, file, remoteSubDir,connectId,host);
            } else {
                uploadFile(sftp, file, remoteDir);
            }

        }
    }

    private static void uploadFile(ChannelSftp sftp, File localFile, String remoteDir) throws SftpException {
        sftp.put(localFile.getAbsolutePath(), remoteDir + "/" + localFile.getName());
    }

    public static HostVo sSHConnection(String host, int port, String username, String password) {
        HostVo hostVo = new HostVo();
        SSHClient sshClient = new SSHClient();
        try {
            sshClient.addHostKeyVerifier(new PromiscuousVerifier());
            sshClient.connect(host, port);
            sshClient.authPassword(username, DESUtil.decrypt(password));
            hostVo.setState(true);
            hostVo.setMsg("连接成功！");
            return hostVo;
        } catch (Exception e) {
            LogUtil.severe("连接失败",e);
            e.printStackTrace();
            hostVo.setState(false);
            hostVo.setMsg(e.getMessage());
            return hostVo;
        } finally {
            try {
                if (sshClient.isConnected()) {
                    sshClient.disconnect();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static List<DirectorFileVo> listDirectories(String host, int port, String username, String password, String directory) {
        List<DirectorFileVo> directories = new ArrayList<>();
        ChannelSftp sftp = null;
        try {
            JSch jsch = new JSch();
            com.jcraft.jsch.Session jschSession = jsch.getSession(username, host, port);
            jschSession.setPassword(DESUtil.decrypt(password));
            Properties config = new Properties();
            config.put("StrictHostKeyChecking", "no");
            jschSession.setConfig(config);
            jschSession.connect();
            sftp = (ChannelSftp) jschSession.openChannel("sftp");
            sftp.connect();
            Vector<ChannelSftp.LsEntry> files = sftp.ls(directory);
            for (ChannelSftp.LsEntry entry : files) {
                if (entry.getAttrs().isDir() && !entry.getFilename().equals(".") && !entry.getFilename().equals("..")) {
                    directories.add(new DirectorFileVo((directory.endsWith("/") ? directory : directory + "/") + entry.getFilename()));

                }
            }
        } catch (JSchException | SftpException e) {
            LogUtil.severe("文件读取失败！",e);
            e.printStackTrace();
        } finally {
            if (sftp != null && sftp.isConnected()) {
                sftp.disconnect();
            }
        }
        return directories;
    }

}
