package com.easy.bean.vo;

import lombok.Data;

/**
 * @author typsusan
 * <p>des</p>
 **/
@Data
public class HostVo {

    private boolean state;

    private String msg;
}
