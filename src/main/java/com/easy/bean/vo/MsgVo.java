package com.easy.bean.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author typsusan
 * <p>des</p>
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MsgVo {

    private String key;

    private String msg;

}
