package com.easy.bean;

import lombok.Data;

/**
 * @author typsusan
 * <p>des</p>
 **/
@Data
public class DataHost {

    /**
     * 服务器id
     */
    private String serverId;

    /**
     * 服务器ip
     */
    private String host;

    /**
     * 账号
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 端口
     */
    private int port;

    /**
     * 名称
     */
    private String name;

    /**
     * 创建时间
     */
    private String createTime;

}
