package com.easy.result;

import lombok.Data;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@Data
public class EasyJenkinsResult {

    /**
     * 返回消息
     */
    private String msg;

    /**
     * 状态码
     */
    private int code;

    /**
     * 返回token
     */
    private String token;

    /**
     * 数据集
     */
    private Object data;

    /**
     * 登录人
     */
    private String username;

    /**
     * 用户类型
     */
    private Integer userType;

    /**
     * 存活时间
     */
    private long userTime;


    /*成功*/
    public static final Integer SUCCESS_CODE = 200;
    /*失败*/
    public static final Integer ERROR = -1;
    /*失效*/
    public static final Integer USER_ERROR = -2;
    /*线路不够异常*/
    public static final Integer READ_ERROR = -3;
    /**
     * 网站错误
     */
    public static final Integer WEB_ERROR = -5;
    public static final String UPDATE_SUCCESS = "修改成功";
    public static final String UPDATE_ERROR = "修改失败";
    public static final String SAVE_SUCCESS = "添加成功";
    public static final String SAVE_ERROR = "添加失败";
    public static final String DELETE_SUCCESS = "删除成功";
    public static final String DELETE_ERROR = "删除失败";

    public EasyJenkinsResult setUserType(Integer userType) {
        this.userType = userType;
        return this;
    }

    public EasyJenkinsResult setUserTime(long userTime) {
        this.userTime = userTime;
        return this;
    }

    public EasyJenkinsResult setCode(int code) {
        this.code = code;
        return this;
    }

    public EasyJenkinsResult setUsername(String username) {
        this.username = username;
        return this;
    }

    public EasyJenkinsResult setData(Object data) {
        this.data = data;
        return this;
    }

    public EasyJenkinsResult setToken(String token) {
        this.token = token;
        return this;
    }

    public EasyJenkinsResult setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public static EasyJenkinsResult saveSuccess(){
        return new EasyJenkinsResult().setCode(EasyJenkinsResult.SUCCESS_CODE).setMsg(EasyJenkinsResult.SAVE_SUCCESS);
    }
    public static EasyJenkinsResult saveError(){
        return new EasyJenkinsResult().setCode(EasyJenkinsResult.ERROR).setMsg(EasyJenkinsResult.SAVE_ERROR);
    }

    public static EasyJenkinsResult save(boolean save){
        return save?saveSuccess():saveError();
    }

    public static EasyJenkinsResult updateSuccess(){
        return new EasyJenkinsResult().setCode(EasyJenkinsResult.SUCCESS_CODE).setMsg(EasyJenkinsResult.UPDATE_SUCCESS);
    }
    public static EasyJenkinsResult updateError(){
        return new EasyJenkinsResult().setCode(EasyJenkinsResult.ERROR).setMsg(EasyJenkinsResult.UPDATE_ERROR);
    }

    public static EasyJenkinsResult update(boolean update){
        return update?updateSuccess():updateError();
    }

    public static EasyJenkinsResult deleteSuccess(){
        return new EasyJenkinsResult().setCode(EasyJenkinsResult.SUCCESS_CODE).setMsg(EasyJenkinsResult.DELETE_SUCCESS);
    }
    public static EasyJenkinsResult deleteError(){
        return new EasyJenkinsResult().setCode(EasyJenkinsResult.ERROR).setMsg(EasyJenkinsResult.DELETE_ERROR);
    }

    public static EasyJenkinsResult delete(boolean delete){
        return delete?deleteSuccess():deleteError();
    }

    public static EasyJenkinsResult success(int code, String msg,String token,Object data){
        return new EasyJenkinsResult().setCode(code).setMsg(msg).setToken(token).setData(data);
    }

    public static EasyJenkinsResult success(int code, String msg,String token,String username){
        return new EasyJenkinsResult().setCode(code).setMsg(msg).setToken(token).setUsername(username);
    }

    public static EasyJenkinsResult success(int code, String msg,String token,String username,Integer userType,long userTime){
        return new EasyJenkinsResult().setCode(code).setMsg(msg).setToken(token).setUsername(username).setUserType(userType).setUserTime(userTime);
    }

    public static EasyJenkinsResult error(int code, String msg){
        return new EasyJenkinsResult().setCode(code).setMsg(msg);
    }
    public static EasyJenkinsResult error(String msg){
        return new EasyJenkinsResult().setMsg(msg).setCode(EasyJenkinsResult.ERROR);
    }

    public static EasyJenkinsResult error(int code){
        return new EasyJenkinsResult().setCode(code);
    }

    public static EasyJenkinsResult success(){
        return new EasyJenkinsResult().setCode(SUCCESS_CODE);
    }
    public static EasyJenkinsResult success(Object data){
        return new EasyJenkinsResult().setCode(SUCCESS_CODE).setData(data);
    }
    public static EasyJenkinsResult success(int code,String msg){
        return new EasyJenkinsResult().setCode(code).setMsg(msg);
    }
}
