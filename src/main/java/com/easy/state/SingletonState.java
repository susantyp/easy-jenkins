package com.easy.state;

import com.easy.deploy.util.LogUtil;
import com.easy.enums.EasyJenkinsEnum;
import lombok.extern.slf4j.Slf4j;

/**
 * @author typsusan
 * <p>des</p>
 **/
@Slf4j
public class SingletonState {
    private static volatile SingletonState instance = null;

    /**
     * 启动状态
     */
    private boolean flag;



    private SingletonState() {
        this.flag = false;
    }

    public static SingletonState getInstance() {
        if (instance == null) {
            synchronized (SingletonState.class) {
                if (instance == null) {
                    instance = new SingletonState();
                }
            }
        }
        return instance;
    }

    public boolean getFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public void setFlag(boolean flag, String id, String host) {
        this.flag = flag;
        if (!flag){
            LogUtil.info(EasyJenkinsEnum.SUCCESSFULLY_DEPLOYED.getParam(),id,host);
        }
    }

}
