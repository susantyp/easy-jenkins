function springbootCommand(serverPath,jarName,name) {
    return [

        // 默认提示命令
        { "value": "nohup java -jar "+pathIsNull(serverPath)+"/"+jarNameIsNull(jarName)+" > "+pathIsNull(serverPath)+"/nohup.out 2>&1 & " },
        { "value": "nohup java -jar "+pathIsNull(serverPath)+"/"+jarNameIsNull(jarName)+" &" },
        { "value": "nohup java -jar "+pathIsNull(serverPath)+"/"+jarNameIsNull(jarName)+" > "+nameIsNull(name)+".log &" },
        { "value": "nohup java -jar "+pathIsNull(serverPath)+"/"+jarNameIsNull(jarName)+" > "+pathIsNull(serverPath)+"/"+nameIsNull(name)+".log &" },
        { "value": "java -jar "+pathIsNull(serverPath)+"/"+jarNameIsNull(jarName)+"" }

        // 增加 更多提示命令
    ];
}


function jarNameIsNull(name){
    if (!name){
        return 'xxx.jar'
    }else {
        return name;
    }
}

function pathIsNull(path){
    if (!path){
        return '/home'
    }else {
        return path;
    }
}

function nameIsNull(name){
    if (!name){
        return "log"
    }else {
        return name;
    }
}