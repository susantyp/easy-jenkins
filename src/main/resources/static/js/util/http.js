// http.js
const $http = {
    request: async (url, method = 'GET', data = null, headers = {}) => {
        try {
            // 设置基本的 headers
            const defaultHeaders = {
                'Content-Type': 'application/json'
            };

            // 配置请求选项
            const options = {
                method,
                headers: { ...defaultHeaders, ...headers }
            };

            // 如果是 POST 或 PUT 请求，并且有数据，则添加到请求体中
            if ((method === 'POST' || method === 'PUT') && data) {
                options.body = JSON.stringify(data);
            }

            // 发送请求
            const response = await fetch(url, options);

            // 检查响应状态
            if (!response.ok) {
                throw new Error(`HTTP error! status: ${response.status}`);
            }

            // 如果响应体不为空，则尝试解析为 JSON
            return response.json();
        } catch (error) {
            console.error('Request failed:', error);
            throw error;
        }
    },

    get: async (url, headers = {}) => {
        return await $http.request(url, 'GET', null, headers);
    },

    post: async (url, data, headers = {}) => {
        return await $http.request(url, 'POST', data, headers);
    },

    put: async (url, data, headers = {}) => {
        return await $http.request(url, 'PUT', data, headers);
    },

    delete: async (url, headers = {}) => {
        return await $http.request(url, 'DELETE', null, headers);
    }
}
