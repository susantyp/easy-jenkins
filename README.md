<p align="center">
    <img src="./logo.png" width="380" height="140" alt="easy-jenkins logo" title="Easy Jenkins - 一键部署工具" />
</p>

<h1 align="center">Easy Jenkins</h1>

<p align="center">一键部署工具</p>

<p align="center">
  <a href="#"><img src="https://img.shields.io/badge/Spring%20Boot-2.7.5-brightgreen.svg" alt="Spring Boot 2.7.5"></a>
  <a href="#"><img src="https://img.shields.io/badge/JSch-0.1.53-blue.svg" alt="JSch 0.1.53"></a>
  <a href="#"><img src="https://img.shields.io/badge/SSHJ-0.29.0-lightgrey.svg" alt="SSHJ 0.29.0"></a>
  <a href="#"><img src="https://img.shields.io/badge/Thymeleaf-3.0.15-brightgreen.svg" alt="Thymeleaf 3.0.15"></a>
</p>


--- 


<p align="center">一款针对 Vue 和 Jar 的简易部署工具，提供流水线式的一键部署体验。</p>

## 🚀 介绍

Easy Jenkins 是一款操作简单，实现一键部署的工具。它采用流水线形式架构，提供实时的部署过程和记录。界面友好简洁，使用方便，适合日常开发需求。支持分支管理，无需登录，默认分支为 jenkins。每个分支可以配置多个数据源，通过切换不同分支管理不同数据源。Easy Jenkins 采用本地存储结构，无需配置数据库，简单易上手。

## 📦 代码拉取

请使用以下命令拉取最新 **V2.0** 版本的代码，更新于：2024年1月22日。

```bash
git clone https://gitee.com/susantyp/easy-jenkins.git
```

## 📘 教程

详细使用教程请参见：[easy-jenkins 详细使用教程](https://tanyongpeng.blog.csdn.net/article/details/128223343)

## ✨ 功能点

- 部署列表
- 协议列表
- 部署记录
- 数据分支管理
- 基本设置

## 📋 使用说明

主启动类为 `EasyJenkinsApplication`。启动完成后，请按照安装向导填写相应的配置信息。

## 📸 效果预览

- 部署列表
  ![部署列表](home.jpg)
- 协议列表
  ![部署列表](host.jpg)
- 部署记录
  ![部署记录](record.jpg)
- 数据分支
  ![数据分支](branch.jpg)
- 基本设置
  ![基本设置](set.jpg)

## 🤝 贡献

欢迎通过 Pull Requests 或 Issues 参与贡献，让项目更加完美。

## 📄 许可证

本项目采用 [MIT 许可证](LICENSE)。

## 📬 联系方式

如有任何问题或建议，请通过 [Gitee Issues](https://gitee.com/susantyp/easy-jenkins/issues) 联系我们。